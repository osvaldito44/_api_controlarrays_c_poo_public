/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var menudata={children:[
{text:"Página principal",url:"index.html"},
{text:"Páginas relacionadas",url:"pages.html"},
{text:"Estructuras de Datos",url:"annotated.html",children:[
{text:"Estructura de datos",url:"annotated.html"},
{text:"Índice de estructura de datos",url:"classes.html"},
{text:"Campos de datos",url:"functions.html",children:[
{text:"Todo",url:"functions.html",children:[
{text:"a",url:"functions.html#index_a"},
{text:"c",url:"functions.html#index_c"},
{text:"e",url:"functions.html#index_e"},
{text:"f",url:"functions.html#index_f"},
{text:"g",url:"functions.html#index_g"},
{text:"p",url:"functions.html#index_p"},
{text:"s",url:"functions.html#index_s"}]},
{text:"Variables",url:"functions_vars.html",children:[
{text:"a",url:"functions_vars.html#index_a"},
{text:"c",url:"functions_vars.html#index_c"},
{text:"e",url:"functions_vars.html#index_e"},
{text:"f",url:"functions_vars.html#index_f"},
{text:"g",url:"functions_vars.html#index_g"},
{text:"p",url:"functions_vars.html#index_p"},
{text:"s",url:"functions_vars.html#index_s"}]}]}]},
{text:"Archivos",url:"files.html",children:[
{text:"Lista de archivos",url:"files.html"},
{text:"Globales",url:"globals.html",children:[
{text:"Todo",url:"globals.html"},
{text:"Funciones",url:"globals_func.html"},
{text:"typedefs",url:"globals_type.html"},
{text:"defines",url:"globals_defs.html"}]}]}]}
