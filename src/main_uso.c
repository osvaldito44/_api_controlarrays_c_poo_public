/*
* Este programa sirve para verificar el funcionamiento del API "arreglo_lista.so"
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "arreglo_lista.h"

int main(int argc, char const *argv[]){
	printf("Para un arreglo Bidi de tipo int");
	int **arr_cop_uni = NULL;	
	int i, j;
	classArregloBidi *obj = new_ObjArreglo();
	classArregloBidi *ob2 = new_ObjArreglo();
	assert(obj != NULL && ob2 != NULL);

	obj->setFilasColumnasInt(obj, 10, 10);	
	printf("\n");
	obj->setFilasColumnasInt(obj, 0, 10);
	printf("\n");
	obj->setFilasColumnasInt(obj, -1, -1);
	printf("\n");
	obj->setFilasColumnasInt(obj, 32767, 20);
	printf("\n");
	obj->setFilasColumnasInt(obj, 32766, 10);
	printf("\n");
	obj->setFilasColumnasInt(obj, 3, 6);
	printf("\n");
	obj->printArrayBidiInt(obj);
	printf("\n");
	
	obj->setValorArrayBidiInt(obj, -2, -2, 5);
	printf("\n");
	obj->setValorArrayBidiInt(obj, -1, -1, 5);
	printf("\n");
	obj->setValorArrayBidiInt(obj, 0, 0, 1);
	printf("\n");
	obj->setValorArrayBidiInt(obj, 1, 1, 2);
	printf("\n");
	obj->setValorArrayBidiInt(obj, 2, 2, 3);
	printf("\n");
	obj->setValorArrayBidiInt(obj, 3, 3, 4);
	printf("\n");
	obj->setValorArrayBidiInt(obj, 5, 5, 5);
	printf("\n");
	obj->setValorArrayBidiInt(obj, 6, 6, 6);
	printf("\n");
	obj->setValorArrayBidiInt(obj, 7, 7, 7);
	printf("\n");
	printf("Impresión de arreglo despues de meter valores....\n");
	obj->printArrayBidiInt(obj);
	printf("\n");
	
	printf("\tPrimera parte bien..\n\n");	
	
	arr_cop_uni = obj->getArrayBidiInt(obj);
	int filas = obj->getFilasArrayBidiInt(obj);			
	int columnas = obj->getColumnasArrayBidiInt(obj);
	printf("filas: %d\n", filas);
	printf("columnas: %d\n", columnas);
	printf("Impresión de arreglo uni: \n");	
	for(i = 0; i < filas; i++){
	  for(j = 0; j < columnas; j++){
	    printf(" %d ", arr_cop_uni[i][j]);
	  }
	  printf("\n");
	}
	printf("\n");
	arr_cop_uni[1][4] = 100;
	
	ob2->setArrayBidiInt(ob2, arr_cop_uni, filas, columnas);
	printf("\tImprimiendo a ob2 con copia de arr_cop_uni\n");
	ob2->printArrayBidiInt(ob2);
	
	printf("\n\tSegunda parte bien..\n\n");			
	obj->setArrayBidiInt(obj, arr_cop_uni, obj->getFilasArrayBidiInt(obj), obj->getColumnasArrayBidiInt(obj));
	printf("Imprimiendo de nuevo el arreglo\n");
	obj->printArrayBidiInt(obj);
	printf("\n");
	int valor = obj->getValorArrayBidiInt(obj, -2, -2);
	printf("Valor = %d\n", valor);
	valor = obj->getValorArrayBidiInt(obj, -1, -1);
	printf("Valor = %d\n", valor);
	valor = obj->getValorArrayBidiInt(obj, 0, 0);
	printf("Valor = %d\n", valor);
	valor = obj->getValorArrayBidiInt(obj, 1, 2);
	printf("Valor = %d\n", valor);
	valor = obj->getValorArrayBidiInt(obj, 2, 2);
	printf("Valor = %d\n", valor);
	valor = obj->getValorArrayBidiInt(obj, 3, 3);
	printf("Valor = %d\n", valor);
	valor = obj->getValorArrayBidiInt(obj, 4, 4);
	printf("Valor = %d\n", valor);
	valor = obj->getValorArrayBidiInt(obj, 5, 5);
	printf("Valor = %d\n", valor);
	valor = obj->getValorArrayBidiInt(obj, 6, 6);
	printf("Valor = %d\n", valor);	
	
	ob2->freeArrayBidiInt(ob2);
	printf("\tImprimiendo a ob2....\n");
	ob2->printArrayBidiInt(ob2);
	
			
	obj->freeArrayBidiInt(obj);	
	ob2->freeArrayBidiInt(ob2);
	printf("\n");
	obj->printArrayBidiInt(obj);
	printf("\n");
	free_ObjArreglo(obj);
	free_ObjArreglo(ob2);
	
	/*
	printf("Para un arreglo Bidi de tipo float");
	float **arr_cop_uni = NULL;	
	int i, j;
	classArregloBidi *obj = new_ObjArreglo();
	classArregloBidi *ob2 = new_ObjArreglo();
	assert(obj != NULL && ob2 != NULL);

	obj->setFilasColumnasFloat(obj, 10, 10);	
	printf("\n");
	obj->setFilasColumnasFloat(obj, 0, 10);
	printf("\n");
	obj->setFilasColumnasFloat(obj, -1, -1);
	printf("\n");
	obj->setFilasColumnasFloat(obj, 32767, 20);
	printf("\n");
	obj->setFilasColumnasFloat(obj, 32766, 10);
	printf("\n");
	obj->setFilasColumnasFloat(obj, 3, 6);
	printf("\n");
	obj->printArrayBidiFloat(obj);
	printf("\n");
	
	obj->setValorArrayBidiFloat(obj, -2, -2, 5.0);
	printf("\n");
	obj->setValorArrayBidiFloat(obj, -1, -1, 5.0);
	printf("\n");
	obj->setValorArrayBidiFloat(obj, 0, 0, 1.0);
	printf("\n");
	obj->setValorArrayBidiFloat(obj, 1, 1, 2.0);
	printf("\n");
	obj->setValorArrayBidiFloat(obj, 2, 2, 3.0);
	printf("\n");
	obj->setValorArrayBidiFloat(obj, 3, 3, 4.0);
	printf("\n");
	obj->setValorArrayBidiFloat(obj, 5, 5, 5.0);
	printf("\n");
	obj->setValorArrayBidiFloat(obj, 6, 6, 6.0);
	printf("\n");
	obj->setValorArrayBidiFloat(obj, 7, 7, 7.0);
	printf("\n");
	printf("Impresión de arreglo despues de meter valores....\n");
	obj->printArrayBidiFloat(obj);
	printf("\n");
	
	printf("\tPrimera parte bien..\n\n");	
	
	arr_cop_uni = obj->getArrayBidiFloat(obj);
	int filas = obj->getFilasArrayBidiFloat(obj);			
	int columnas = obj->getColumnasArrayBidiFloat(obj);
	printf("filas: %d\n", filas);
	printf("columnas: %d\n", columnas);
	printf("Impresión de arreglo uni: \n");	
	for(i = 0; i < filas; i++){
	  for(j = 0; j < columnas; j++){
	    printf(" %f ", arr_cop_uni[i][j]);
	  }
	  printf("\n");
	}
	printf("\n");
	arr_cop_uni[1][4] = 100.0;
	
	ob2->setArrayBidiFloat(ob2, arr_cop_uni, filas, columnas);
	printf("\tImprimiendo a ob2 con copia de arr_cop_uni\n");
	ob2->printArrayBidiFloat(ob2);
	
	printf("\n\tSegunda parte bien..\n\n");			
	obj->setArrayBidiFloat(obj, arr_cop_uni, obj->getFilasArrayBidiFloat(obj), obj->getColumnasArrayBidiFloat(obj));
	printf("Imprimiendo de nuevo el arreglo\n");
	obj->printArrayBidiFloat(obj);
	printf("\n");
	float valor = obj->getValorArrayBidiFloat(obj, -2, -2);
	printf("Valor = %f\n", valor);
	valor = obj->getValorArrayBidiFloat(obj, -1, -1);
	printf("Valor = %f\n", valor);
	valor = obj->getValorArrayBidiFloat(obj, 0, 0);
	printf("Valor = %f\n", valor);
	valor = obj->getValorArrayBidiFloat(obj, 1, 2);
	printf("Valor = %f\n", valor);
	valor = obj->getValorArrayBidiFloat(obj, 2, 2);
	printf("Valor = %f\n", valor);
	valor = obj->getValorArrayBidiFloat(obj, 3, 3);
	printf("Valor = %f\n", valor);
	valor = obj->getValorArrayBidiFloat(obj, 4, 4);
	printf("Valor = %f\n", valor);
	valor = obj->getValorArrayBidiFloat(obj, 5, 5);
	printf("Valor = %f\n", valor);
	valor = obj->getValorArrayBidiFloat(obj, 6, 6);
	printf("Valor = %f\n", valor);	
	
	ob2->freeArrayBidiFloat(ob2);
	printf("\tImprimiendo a ob2....\n");
	ob2->printArrayBidiFloat(ob2);
	
			
	obj->freeArrayBidiFloat(obj);	
	ob2->freeArrayBidiFloat(ob2);
	printf("\n");
	obj->printArrayBidiFloat(obj);
	printf("\n");
	free_ObjArreglo(obj);
	free_ObjArreglo(ob2);
	*/
	
	/*printf("Para un arreglo Uni de tipo float");
	float *arr_cop_uni = NULL;	
	int i;
	classLista *obj = new_ObjLista();
	classLista *ob2 = new_ObjLista();
	assert(obj != NULL && ob2 != NULL);

	obj->setElementosListaFloat(obj, 10);
	printf("\n");
	obj->setElementosListaFloat(obj, 0);
	printf("\n");
	obj->setElementosListaFloat(obj, -1);
	printf("\n");
	obj->setElementosListaFloat(obj, 32767);
	printf("\n");
	obj->setElementosListaFloat(obj, 32766);
	printf("\n");
	obj->setElementosListaFloat(obj, 6);
	printf("\n");
	obj->printListaFloat(obj);
	printf("\n");
	
	obj->setValorListaFloat(obj, -2, 5.0);
	printf("\n");
	obj->setValorListaFloat(obj, -1, 5.0);
	printf("\n");
	obj->setValorListaFloat(obj, 0, 1.0);
	printf("\n");
	obj->setValorListaFloat(obj, 1, 2.0);
	printf("\n");
	obj->setValorListaFloat(obj, 2, 3.0);
	printf("\n");
	obj->setValorListaFloat(obj, 3, 4.0);
	printf("\n");
	obj->setValorListaFloat(obj, 5, 5.0);
	printf("\n");
	obj->setValorListaFloat(obj, 6, 6.0);
	printf("\n");
	obj->setValorListaFloat(obj, 7, 7.0);
	printf("\n");
	printf("Impresión de arreglo despues de meter valores....\n");
	obj->printListaFloat(obj);
	printf("\n");
	
	printf("\tPrimera parte bien..\n\n");	
	
	arr_cop_uni = obj->getListaFloat(obj);
	int total_elementos = obj->getTotalElementosListaFloat(obj);			
	printf("Total de elementos: %d\n", total_elementos);
	printf("Impresión de arreglo uni: \n");	
	for(i = 0; i < total_elementos; i++){
		printf(" %f ", arr_cop_uni[i]);
	}
	printf("\n");
	arr_cop_uni[3] = 100.0;
	
	ob2->setListaFloat(ob2, arr_cop_uni, total_elementos);
	printf("\tImprimiendo a ob2 con copia de arr_cop_uni\n");
	ob2->printListaFloat(ob2);
	
	printf("\n\tSegunda parte bien..\n\n");	
	
	obj->setListaFloat(obj, arr_cop_uni, obj->getTotalElementosListaFloat(obj));
	printf("Imprimiendo de nuevo el arreglo\n");
	obj->printListaFloat(obj);
	printf("\n");
	float valor = obj->getValorListaFloat(obj, -2);
	printf("Valor = %f\n", valor);
	valor = obj->getValorListaFloat(obj, -1);
	printf("Valor = %f\n", valor);
	valor = obj->getValorListaFloat(obj, 0);
	printf("Valor = %f\n", valor);
	valor = obj->getValorListaFloat(obj, 1);
	printf("Valor = %f\n", valor);
	valor = obj->getValorListaFloat(obj, 2);
	printf("Valor = %f\n", valor);
	valor = obj->getValorListaFloat(obj, 3);
	printf("Valor = %f\n", valor);
	valor = obj->getValorListaFloat(obj, 4);
	printf("Valor = %f\n", valor);
	valor = obj->getValorListaFloat(obj, 5);
	printf("Valor = %f\n", valor);
	valor = obj->getValorListaFloat(obj, 6);
	printf("Valor = %f\n", valor);	
	
	ob2->freeListaFloat(ob2);
	printf("\tImprimiendo a ob2....\n");
	ob2->printListaFloat(ob2);
	
			
	obj->freeListaFloat(obj);	
	ob2->freeListaFloat(ob2);
	printf("\n");
	obj->printListaFloat(obj);
	printf("\n");
	free_ObjLista(obj);
	free_ObjLista(ob2);
	*/
	
	/*
	printf("Para un arreglo Uni de tipo int");
	int *arr_cop_uni = NULL;	
	int i;
	classLista *obj = new_ObjLista();
	classLista *ob2 = new_ObjLista();
	assert(obj != NULL && ob2 != NULL);

	obj->setElementosListaInt(obj, 10);
	printf("\n");
	obj->setElementosListaInt(obj, 0);
	printf("\n");
	obj->setElementosListaInt(obj, -1);
	printf("\n");
	obj->setElementosListaInt(obj, 32767);
	printf("\n");
	obj->setElementosListaInt(obj, 32766);
	printf("\n");
	obj->setElementosListaInt(obj, 6);
	printf("\n");
	obj->printListaInt(obj);
	printf("\n");
	
	obj->setValorListaInt(obj, -2, 5);
	printf("\n");
	obj->setValorListaInt(obj, -1, 5);
	printf("\n");
	obj->setValorListaInt(obj, 0, 1);
	printf("\n");
	obj->setValorListaInt(obj, 1, 2);
	printf("\n");
	obj->setValorListaInt(obj, 2, 3);
	printf("\n");
	obj->setValorListaInt(obj, 3, 4);
	printf("\n");
	obj->setValorListaInt(obj, 5, 5);
	printf("\n");
	obj->setValorListaInt(obj, 6, 6);
	printf("\n");
	obj->setValorListaInt(obj, 7, 7);
	printf("\n");
	printf("Impresión de arreglo despues de meter valores....\n");
	obj->printListaInt(obj);
	printf("\n");
	
	printf("\tPrimera parte bien..\n\n");	
	
	arr_cop_uni = obj->getListaInt(obj);
	int total_elementos = obj->getTotalElementosListaInt(obj);			
	printf("Total de elementos: %d\n", total_elementos);
	printf("Impresión de arreglo uni: \n");	
	for(i = 0; i < total_elementos; i++){
		printf(" %d ", arr_cop_uni[i]);
	}
	printf("\n");
	arr_cop_uni[3] = 100;
	
	ob2->setListaInt(ob2, arr_cop_uni, total_elementos);
	printf("\tImprimiendo a ob2 con copia de arr_cop_uni\n");
	ob2->printListaInt(ob2);
	
	printf("\n\tSegunda parte bien..\n\n");	
	
	obj->setListaInt(obj, arr_cop_uni, obj->getTotalElementosListaInt(obj));
	printf("Imprimiendo de nuevo el arreglo\n");
	obj->printListaInt(obj);
	printf("\n");
	int valor = obj->getValorListaInt(obj, -2);
	printf("Valor = %d\n", valor);
	valor = obj->getValorListaInt(obj, -1);
	printf("Valor = %d\n", valor);
	valor = obj->getValorListaInt(obj, 0);
	printf("Valor = %d\n", valor);
	valor = obj->getValorListaInt(obj, 1);
	printf("Valor = %d\n", valor);
	valor = obj->getValorListaInt(obj, 2);
	printf("Valor = %d\n", valor);
	valor = obj->getValorListaInt(obj, 3);
	printf("Valor = %d\n", valor);
	valor = obj->getValorListaInt(obj, 4);
	printf("Valor = %d\n", valor);
	valor = obj->getValorListaInt(obj, 5);
	printf("Valor = %d\n", valor);
	valor = obj->getValorListaInt(obj, 6);
	printf("Valor = %d\n", valor);	
	
	ob2->freeListaInt(ob2);
	printf("\tImprimiendo a ob2....\n");
	ob2->printListaInt(ob2);
	
			
	obj->freeListaInt(obj);	
	ob2->freeListaInt(ob2);
	printf("\n");
	obj->printListaInt(obj);
	printf("\n");
	free_ObjLista(obj);
	free_ObjLista(ob2);
	*/

	return 0;
}