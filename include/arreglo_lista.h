/**
 * @file    arreglo_lista.h
 * @brief   Cabecera del fichero arreglo_lista.c
 * @par		Declaraciones 
 *			- classArregloBidi Definicion de la clase para crear un objeto que permita el control de arrays bidi float/int.
 *			- classAttArregloBidi Definicion de la estructura para los atributos de la clase classArregloBidi.
 *			- classLista Definicion de la clase para crear un objeto que permita el control de arrays uni float/int.
 *			- classAttLista Definición de la estructura para los atributos de la clase classLista. 
 * @author  F.Mercado
 * @date    2016-11-09 
 */

#ifndef ARREGLO_LISTA_H
#define ARREGLO_LISTA_H
 
#include <stdio.h>
#include <malloc.h>

#define TAM_ELEMENTOS 32767
 
typedef struct ARREGLO_BIDI classArregloBidi; // Clase ArregloBidi
typedef struct CLASS_ATT_ARREGLO_BIDI classAttArregloBidi; // Atributos de la Clase Arreglo
typedef struct LISTA classLista; // Clase Lista
typedef struct CLASS_ATT_LISTA classAttLista; // Atributos de la Clase Lista
 
// Definición de la clase Arreglo
struct ARREGLO_BIDI{
	/**
 	* @brief   *atributos --> define los atributos de la clase ARREGLO_BIDI
 	*/
	classAttArregloBidi *atributos;
 	// ----> Para un array bidimensional de tipo float
 	/**
 	* @brief   Define el número de filas y columnas tendrá el array bidi de tipo float, sí existe un error con los límites retorna un mensaje de error.
 	* @param   *this Objeto de tipo classArregloBidi
 	* @param   filas valor de tipo int que define el número de filas para el array bidimensional de tipo float
 	* @param   columnas valor de tipo int que define el número de columnas para el array bidimensional de tipo float
 	* @return  void
 	*/
 	// Define el número de columnas y filas para un array bidi float
 	void (*setFilasColumnasFloat)(classArregloBidi *this, int filas, int columnas);
 	/**
 	* @brief   Define el número de filas tendrá el array bidi de tipo float, sí existe un error con el límite de filas retorna un mensaje de error.
 	* @param   *this Objeto de tipo classArregloBidi
 	* @param   filas valor de tipo int que define el número de filas para el array bidimensional de tipo float
 	* @return  void
 	*/
 	// Define el número de filas para un array bidi float
 	void (*setFilasFloat)(classArregloBidi *this, int filas);
 	/**
 	* @brief   Define el número de columnas tendrá el array bidi de tipo float, sí existe un error con el límite de columnas retorna un mensaje de error.
 	* @param   *this Objeto de tipo classArregloBidi
 	* @param   columnas valor de tipo int que define el número de columnas para el array bidimensional de tipo float
 	* @return  void
 	*/
 	// Define el número de columnas para un array bidi float
 	void (*setColumnasFloat)(classArregloBidi *this, int columnas);
 	/**
 	* @brief   Se obtiene un array bidi de tipo float (ejemplo: float **array_bidi)
 	* @param   *this Objeto de tipo classArregloBidi
 	* @return  array_bidi --> es el array bidi de tipo float que se retornará
 	*/
 	// Se obtiene un array bidi de tipo float
 	float **(*getArrayBidiFloat)(classArregloBidi *this);
 	/**
 	* @brief   Se define de una sola vez un array bidi de tipo float
 	* @param   *this Objeto de tipo classArregloBidi
 	* @param   **array_bidi --> puntero hacia un puntero de tipo float, array_bidi es de entrada
	* @param   filas valor de tipo de int que define el número de filas del arreglo float **array_bidi
	* @param   columnas valor de tipo de int que define el número de columnas del arreglo float **array_bidi
 	* @return  void
	* @todo    obtener de manera transparente el tamaño de filas y columnas de float **array_bidi
 	*/
 	// Se define de una sola vez un array bidi de tipo float (array_bidi es de entrada)
 	void (*setArrayBidiFloat)(classArregloBidi *this, float **array_bidi, int filas, int columnas);
 	/**
 	* @brief   Se agrega un valor float al array bidi de tipo float, sí existe un error con los límites retorna un mensaje de error.
 	* @param   *this Objeto de tipo classArregloBidi
 	* @param   ind_fila valor de tipo int que identifica la fila a acceder
 	* @param   ind_columna valor de tipo int que identica la columna a accdeder
 	* @param   valor elemento de tipo float que se ingresará al array
 	* @return  void
 	*/
 	// Se agrega un valor float al array bidi de tipo float
 	void (*setValorArrayBidiFloat)(classArregloBidi *this, int ind_fila, int ind_columna, float valor);
 	/**
 	* @brief   Se obtiene un valor float del array bidi de tipo float, sí existe un error con los límites retorna un mensaje de error.
 	* @param   *this Objeto de tipo classArregloBidi
 	* @param   ind_fila valor de tipo int que identifica la fila a acceder
 	* @param   ind_columna valor de tipo int que identica la columna a accdeder
 	* @return  valor --> elemento de tipo float que se retornará, sí los indices estan mal se retorna -1.0
 	*/
 	// Se obtiene un valor float del array bidi de tipo float 
 	float (*getValorArrayBidiFloat)(classArregloBidi *this, int ind_fila, int ind_columna);
 	/**
 	* @brief   Se libera la memoria ocupada de un array bidi de tipo float 	
 	* @param   *this Objeto de tipo classArregloBidi
 	* @return  void
 	*/
 	// Se libera la memoria de un array bidi de tipo float 	
	void (*freeArrayBidiFloat)(classArregloBidi *this);
	/**
 	* @brief   Imprimir el array bidi de tipo float
 	* @param   *this Objeto de tipo classArregloBidi
 	* @return  void
 	*/
	// Imprimir el array bidi de tipo float
	void (*printArrayBidiFloat)(classArregloBidi *this);
	/**
 	* @brief   Obtiene el número de filas del array bidi float
 	* @param   *this Objeto de tipo classArregloBidi
 	* @return  filas --> valor de tipo int que retorna el número de filas del array
 	*/
	// Obtener el numero de filas del array bidi float
	int (*getFilasArrayBidiFloat)(classArregloBidi *this);
	/**
 	* @brief   Obtiene el número de columnas del array bidi float
 	* @param   *this Objeto de tipo classArregloBidi
 	* @return  columnas --> valor de tipo int que retorna el número de columnas del array
 	*/
	// Obtener el numero de columnas del array bidi float
	int (*getColumnasArrayBidiFloat)(classArregloBidi *this);
	
	// ----> Para un array bidimensional de tipo int
	/**
 	* @brief   Define el número de filas y columnas tendrá el array bidi de tipo int, sí existe un error con los límites retorna un mensaje de error.
 	* @param   *this Objeto de tipo classArregloBidi
 	* @param   filas valor de tipo int que define el número de filas para el array bidimensional de tipo int
 	* @param   columnas valor de tipo int que define el número de columnas para el array bidimensional de tipo int
 	* @return  void
 	*/
	// Define el número de columnas y filas para un array bidi int
 	void (*setFilasColumnasInt)(classArregloBidi *this, int filas, int columnas);
 	/**
 	* @brief   Define el número de filas tendrá el array bidi de tipo int
 	* @param   *this Objeto de tipo classArregloBidi
 	* @param   filas valor de tipo int que define el número de filas para el array bidimensional de tipo int
 	* @return  void
 	*/
 	// Define el número de filas para un array bidi int
 	void (*setFilasInt)(classArregloBidi *this, int filas);
 	/**
 	* @brief   Define el número de columnas tendrá el array bidi de tipo int
 	* @param   *this Objeto de tipo classArregloBidi
 	* @param   columnas valor de tipo int que define el número de columnas para el array bidimensional de tipo int
 	* @return  void
 	*/
 	// Define el número de columnas para un array bidi int
 	void (*setColumnasInt)(classArregloBidi *this, int columnas);
 	/**
 	* @brief   Se obtiene un array bidi de tipo int (ejemplo: int **array_bidi)
 	* @param   *this Objeto de tipo classArregloBidi
 	* @return  array_bidi --> es el array bidi de tipo int que se retornará
 	*/
 	// Se obtiene un array bidi de tipo int
 	int **(*getArrayBidiInt)(classArregloBidi *this);
 	/**
 	* @brief   Se define de una sola vez un array bidi de tipo int
 	* @param   *this Objeto de tipo classArregloBidi
 	* @param   **array_bidi --> puntero hacia un puntero de tipo int, array_bidi es de entrada
	* @param   filas valor de tipo de int que define el número de filas del arreglo int **array_bidi
	* @param   columnas valor de tipo de int que define el número de columnas del arreglo int **array_bidi
 	* @return  void
	* @todo    obtener de manera transparente el tamaño de filas y columnas de int **array_bidi
 	*/
 	// Se define de una sola vez un array bidi de tipo int (array_bidi es de entrada)
 	void (*setArrayBidiInt)(classArregloBidi *this, int **array_bidi, int filas, int columnas);
 	/**
 	* @brief   Se agrega un valor int al array bidi de tipo int, sí existe un error con los límites retorna un mensaje de error.
 	* @param   *this Objeto de tipo classArregloBidi
 	* @param   ind_fila valor de tipo int que identifica la fila a acceder
 	* @param   ind_columna valor de tipo int que identica la columna a accdeder
 	* @param   valor elemento de tipo int que se ingresará al array, sí los indices estan mal se ingresará 0
 	* @return  void
 	*/
 	// Se agrega un valor int al array bidi de tipo int
 	void (*setValorArrayBidiInt)(classArregloBidi *this, int ind_fila, int ind_columna, int valor);
 	/**
 	* @brief   Se obtiene un valor int del array bidi de tipo int, sí existe un error con los límites retorna un mensaje de error. 
 	* @param   *this Objeto de tipo classArregloBidi
 	* @param   ind_fila valor de tipo int que identifica la fila a acceder
 	* @param   ind_columna valor de tipo int que identica la columna a accdeder
 	* @return  valor --> elemento de tipo int que se retornará, sí los indices estan mal se retorna -1
 	*/
 	// Se obtiene un valor int del array bidi de tipo int 
 	int (*getValorArrayBidiInt)(classArregloBidi *this, int ind_fila, int ind_columna);
 	/**
 	* @brief   Se libera la memoria ocupada de un array bidi de tipo int	
 	* @param   *this Objeto de tipo classArregloBidi
 	* @return  void
 	*/
 	// Se libera la memoria de un array bidi de tipo int 	
	void (*freeArrayBidiInt)(classArregloBidi *this);
	/**
 	* @brief   Imprimir el array bidi de tipo int
 	* @param   *this Objeto de tipo classArregloBidi
 	* @return  void
 	*/
	// Imprimir el array bidi de tipo int
	void (*printArrayBidiInt)(classArregloBidi *this);
	/**
 	* @brief   Obtiene el número de filas del array bidi int
 	* @param   *this Objeto de tipo classArregloBidi
 	* @return  filas --> valor de tipo int que retorna el número de filas del array
 	*/
	// Obtener el numero de filas del array bidi int
	int (*getFilasArrayBidiInt)(classArregloBidi *this);
	/**
 	* @brief   Obtiene el número de columnas del array bidi int
 	* @param   *this Objeto de tipo classArregloBidi
 	* @return  columnas --> valor de tipo int que retorna el número de columnas del array
 	*/
	// Obtener el numero de columnas del array bidi int
	int (*getColumnasArrayBidiInt)(classArregloBidi *this);
};


// Definición de la clase Lista
struct LISTA{
	/**
 	* @brief   *atributos --> define los atributos de la clase LISTA
 	*/
	classAttLista *atributos;
	// ----> Para un array unidimensional de tipo float
	/**
 	* @brief   Define el número de elementos tendrá el array uni de tipo float, sí existe un error con el número de elementos retorna un mensaje de error.
 	* @param   *this Objeto de tipo classLista
 	* @param   elementos valor de tipo int que define el número de elementos para el array
 	* @return  void
 	*/
 	// Define el número de elementos para un array bidi float
 	void (*setElementosListaFloat)(classLista *this, int elementos);
 	/**
 	* @brief   Se obtiene un array uni de tipo float (ejemplo: float *array_uni)
 	* @param   *this Objeto de tipo classLista
 	* @return  array_uni --> es el array uni de tipo float que se retornará
 	*/
 	// Se obtiene un array uni de tipo float
 	float *(*getListaFloat)(classLista *this);
 	/**
 	* @brief   Se define de una sola vez un array uni de tipo float
 	* @param   *this Objeto de tipo classLista
 	* @param   *array_uni puntero hacia un puntero de tipo float, array_uni es de entrada
	* @param   num_elementos valor de tipo int que define el número de elementos del arreglo float *array_uni
 	* @return  void
	* @todo    obtener de manera transparente el numero de elementos del array float *array_uni
 	*/
 	// Se define de una sola vez un array uni de tipo float (array_uni es de entrada)
 	void (*setListaFloat)(classLista *this, float *array_uni, int num_elementos);
 	/**
 	* @brief   Se agrega un valor float al array uni de tipo float, sí existe un error con el índice retorna un mensaje de error.
 	* @param   *this Objeto de tipo classLista
 	* @param   indice valor de tipo int que identifica el elemento a acceder
 	* @param   valor elemento de tipo float que se ingresará al array unidimensional de tipo float
 	* @return  void
 	*/
 	// Se agrega un valor float al array uni de tipo float
 	void (*setValorListaFloat)(classLista *this, int indice, float valor);
 	/**
 	* @brief   Se obtiene un valor float del array uni de tipo float 
 	* @param   *this Objeto de tipo classLista
 	* @param   indice valor de tipo int que identifica el elemento a acceder
 	* @return  valor --> elemento de tipo float que se retornará, sí el indice esta mal se retorna -1.0
 	*/
 	// Se obtiene un valor float del array uni de tipo float 
 	float (*getValorListaFloat)(classLista *this, int indice);
 	/**
 	* @brief   Se libera la memoria ocupada de un array uni de tipo float 	
 	* @param   *this Objeto de tipo classLista
 	* @return  void
 	*/
 	// Se libera la memoria de un array uni de tipo float 	
	void (*freeListaFloat)(classLista *this);
	/**
 	* @brief   Imprimir el array uni de tipo float
 	* @param   *this Objeto de tipo classLista
 	* @return  void
 	*/
	// Imprimir el array uni de tipo float
	void (*printListaFloat)(classLista *this);
	/**
 	* @brief   Obtiene el número de elementos del array uni float
 	* @param   *this Objeto de tipo classLista
 	* @return  elementos --> valor de tipo int que retorna el número de elementos del array
 	*/
	// Obtener el numero de elementos del array uni float
	int (*getTotalElementosListaFloat)(classLista *this);

	// ----> Para un array unidimensional de tipo int
	/**
 	* @brief   Define el número de elementos tendrá el array uni de tipo int, sí existe un error con el número de elementos retorna un mensaje de error.
 	* @param   *this Objeto de tipo classLista
 	* @param   elementos valor de tipo int que define el número de elementos para el array
 	* @return  void
 	*/
 	// Define el número de elementos para un array bidi int
 	void (*setElementosListaInt)(classLista *this, int elementos);
 	/**
 	* @brief   Se obtiene un array uni de tipo int (ejemplo: int *array_uni)
 	* @param   *this Objeto de tipo classLista
 	* @return  array_uni --> es el array uni de tipo int que se retornará
 	*/
 	// Se obtiene un array uni de tipo int
 	int *(*getListaInt)(classLista *this);
 	/**
 	* @brief   Se define de una sola vez un array uni de tipo int
 	* @param   *this Objeto de tipo classLista
 	* @param   *array_uni puntero hacia un puntero de tipo int, array_uni es de entrada
	* @param   num_elementos valor de tipo int que define el número de elementos del arreglo int *array_uni
 	* @return  void
	* @todo    obtener de manera transparente el numero de elementos del array int *array_uni
 	*/
 	// Se define de una sola vez un array uni de tipo int (array_uni es de entrada)
 	void (*setListaInt)(classLista *this, int *array_uni, int num_elementos);
 	/**
 	* @brief   Se agrega un valor float al array uni de tipo int, sí existe un error con el índice retorna un mensaje de error.
 	* @param   *this Objeto de tipo classLista
 	* @param   indice valor de tipo int que identifica el elemento a acceder
 	* @param   valor elemento de tipo int que ingresará al array unidimensional de tipo int
 	* @return  void
 	*/
 	// Se agrega un valor int al array uni de tipo int
 	void (*setValorListaInt)(classLista *this, int indice, int valor);
 	/**
 	* @brief   Se obtiene un valor int del array uni de tipo int 
 	* @param   *this Objeto de tipo classLista
 	* @param   indice valor de tipo int que identifica el elemento a acceder
 	* @return  valor --> elemento de tipo int que se retornará, sí el indice esta mal se retorna -1
 	*/
 	// Se obtiene un valor int del array uni de tipo int 
 	int (*getValorListaInt)(classLista *this, int indice);
 	/**
 	* @brief   Se libera la memoria ocupada de un array uni de tipo int 	
 	* @param   *this Objeto de tipo classLista
 	* @return  void
 	*/
 	// Se libera la memoria de un array uni de tipo int 	
	void (*freeListaInt)(classLista *this);
	/**
 	* @brief   Imprimir el array uni de tipo int
 	* @param   *this Objeto de tipo classLista
 	* @return  void
 	*/
	// Imprimir el array uni de tipo int
	void (*printListaInt)(classLista *this);
	/**
 	* @brief   Obtiene el número de elementos del array uni int
 	* @param   *this Objeto de tipo classLista
 	* @return  elementos --> valor de tipo int que retorna el número de elementos del array
 	*/
	// Obtener el numero de elementos del array uni int
	int (*getTotalElementosListaInt)(classLista *this);
};

// Método para crear un objeto de tipo classArregloBidi
classArregloBidi *new_ObjArreglo();
// Método para eliminar un objeto de tipo classArregloBidi
void free_ObjArreglo(classArregloBidi *this);
// Método para crear un objeto de tipo classLista
classLista *new_ObjLista();
// Método para eliminar un objeto de tipo classLista
void free_ObjLista(classLista *this);
#endif